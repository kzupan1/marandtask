package si.kristian.task.web

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import si.kristian.task.domain.Doctor
import si.kristian.task.dto.DoctorDto
import si.kristian.task.exceptions.DoctorProcessingException
import si.kristian.task.exceptions.DuplicateEntityException
import si.kristian.task.service.DoctorsService
import si.kristian.task.service.DocumentReportsService
import java.util.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/doctors")
class DoctorsController (override val service: DoctorsService, val documentReportsService: DocumentReportsService): GetController<Doctor, DoctorDto>(service) {

    private final val logger = LoggerFactory.getLogger(DoctorsController::class.qualifiedName)!!

    @RequestMapping(method = [RequestMethod.POST], produces = [MediaType.APPLICATION_XML_VALUE], consumes = [MediaType.APPLICATION_XML_VALUE])
    fun add(@RequestBody @Valid doctor: DoctorDto): ResponseEntity<DoctorDto> {
        val executionStartTime = Date()
        try {
            this.service.validateBL(doctor)
            val doctorEntity =  service.insertDoctor(doctor)
            this.documentReportsService.storeReport(doctor.id, executionStartTime)
            return ResponseEntity(this.service.mapToDto(doctorEntity), HttpStatus.CREATED)
        } catch(e: DuplicateEntityException) {
            this.logger.error("Duplicate entity exception", e)
            e.doctorId = doctor.id
            e.executionStartTime = executionStartTime
            throw e
        } catch (e: Exception) {
            this.logger.error("Error in processing add doctor request", e)
            throw DoctorProcessingException(doctor.id, executionStartTime, e.message)
        }
    }
}