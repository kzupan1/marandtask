package si.kristian.task.exceptions

import java.util.*

class DoctorProcessingException(val doctorId: Int, val executionStartTime: Date, message: String?): Exception(message)