package si.kristian.task.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import si.kristian.task.domain.Disease
import si.kristian.task.dto.DiseaseDto
import si.kristian.task.dto.page.DiseasesPageDto
import si.kristian.task.dto.page.DoctorsPageDto
import si.kristian.task.dto.page.PageDto
import si.kristian.task.repository.DiseaseRepository

@Service
class DiseasesService (override val repository: DiseaseRepository): GetService<Disease, DiseaseDto>(repository) {
    override fun mapPageToPageDto(page: Page<Disease>, pageable: Pageable) =
        DiseasesPageDto(page.content.map { e -> this.mapToDto(e) }, pageable.pageNumber, pageable.pageNumber, page.totalElements)


    override fun mapToDto(entity: Disease): DiseaseDto {
        return DiseaseDto(entity.name, entity.id)
    }

    @Transactional
    fun upsertDisease(name: String): Disease {
        val optionalDisease = this.repository.findByName(name)

        return if(optionalDisease.isPresent)
                optionalDisease.get()
            else
                this.repository.save(Disease(name))
    }

}