package si.kristian.task.web

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import si.kristian.task.domain.DocumentReport
import si.kristian.task.dto.DocumentReportDto
import si.kristian.task.service.DocumentReportsService

@RestController
@RequestMapping("/api/document-reports")
class DocumentReportsController(override val service: DocumentReportsService): GetController<DocumentReport, DocumentReportDto>(service)