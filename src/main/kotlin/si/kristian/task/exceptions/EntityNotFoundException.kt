package si.kristian.task.exceptions

class EntityNotFoundException(val id: Int): Exception("Not Found")