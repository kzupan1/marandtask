package si.kristian.task.domain

import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseEntity {
    abstract val id: Int
}