package si.kristian.task.dto

import io.swagger.annotations.ApiModel
import java.io.Serializable
import javax.xml.bind.annotation.*

@ApiModel(value = "disease")
@XmlRootElement(name = "disease")
@XmlAccessorType(XmlAccessType.FIELD)
class DiseaseDto: Serializable, DataTransferObject {

    @XmlValue
    var name: String = ""

    @XmlAttribute
    var id: Int = 0

    constructor() {}
    constructor(name: String) {
        this.name = name
    }
    
    constructor(name: String, id: Int) {
        this.name = name
        this.id = id
    }


}