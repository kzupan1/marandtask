package si.kristian.task.domain

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "document_report")
data class DocumentReport(

        val doctorId: Int,

        @Temporal(TemporalType.TIMESTAMP)
        val executionStartTime: Date,

        @Column(nullable = true)
        val error: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        override val id: Int = 0
): BaseEntity()