package si.kristian.task.domain

import javax.persistence.*

@Entity
data class Doctor(
        @Id
        override val id: Int,

        @Column(nullable = false)
        val department: String,

        @Column(nullable = true)
        @OneToMany(cascade = [CascadeType.ALL])
        var patients: List<Patient>? = null
): BaseEntity()
