package si.kristian.task.dto.errors

import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.*
import javax.xml.bind.annotation.*

@XmlRootElement(name = "error")
@XmlAccessorType(XmlAccessType.FIELD)
internal class ApiError{

    @XmlElement
    var status: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR

    @XmlElement
    val timestamp: Date = Date()

    @XmlElement
    var message: String? = null

    @XmlElement
    var debugMessage: String? = null

    @XmlElementWrapper(name = "subErrors")
    @XmlElement(name = "subError")
    var subErrors: List<ApiSubError>? = null

    constructor() {}

    constructor(status: HttpStatus) {
        this.status = status
    }

    constructor(status: HttpStatus, ex: Throwable) {
        this.status = status
        this.message = "Unexpected error"
        this.debugMessage = ex.localizedMessage
    }

    constructor(status: HttpStatus, message: String, ex: Throwable) {
        this.status = status
        this.message = message
        this.debugMessage = ex.localizedMessage
    }

    constructor(status: HttpStatus, message: String, ex: Throwable, subErrors: List<ApiSubError>) {
        this.status = status
        this.message = message
        this.debugMessage = ex.localizedMessage
        this.subErrors = subErrors
    }


}