package si.kristian.task.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import si.kristian.task.domain.BaseEntity
import si.kristian.task.dto.DataTransferObject
import si.kristian.task.dto.page.PageDto
import si.kristian.task.exceptions.EntityNotFoundException

abstract class GetService<T : BaseEntity, U: DataTransferObject>(open val repository: PagingAndSortingRepository<T, Int>) {

    abstract fun mapToDto(entity: T): U

    abstract fun mapPageToPageDto(page: Page<T>, pageable: Pageable): PageDto<U>

    fun getOne(id: Int): T {
        val result = this.repository.findById(id)

        if (result.isEmpty) {
            throw EntityNotFoundException(id)
        }

        return result.get()
    }

    fun findAll(pageable: Pageable): Page<T> {
       return this.repository.findAll(pageable)
    }

}