package si.kristian.task.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import si.kristian.task.domain.Doctor
import si.kristian.task.dto.DoctorDto
import si.kristian.task.dto.page.DoctorsPageDto
import si.kristian.task.exceptions.DuplicateEntityException
import si.kristian.task.repository.DoctorRepository

@Service
class DoctorsService (override val repository: DoctorRepository,
                      val patientsService: PatientsService): GetService<Doctor, DoctorDto>(repository) {

    override fun mapPageToPageDto(page: Page<Doctor>, pageable: Pageable) =
            DoctorsPageDto(page.content.map { e -> this.mapToDto(e) }, pageable.pageNumber, pageable.pageNumber, page.totalElements)


    override fun mapToDto(entity: Doctor): DoctorDto {
        val patients =
               entity.patients?.map { pe -> this.patientsService.mapToDto(pe) } ?: arrayListOf()
        return DoctorDto(entity.id, entity.department, patients)
    }

    @Throws(DuplicateEntityException::class)
    fun validateBL(doctorDto: DoctorDto) {
        if (this.repository.existsById(doctorDto.id)) {
            throw DuplicateEntityException(doctorDto.id, "Doctor with: ${doctorDto.id} already exists")
        }

        for (patientDto in doctorDto.patients ) {
            this.patientsService.validateBL(patientDto)
        }
    }

    @Transactional
    fun insertDoctor(doctorDto: DoctorDto): Doctor {
        return this.repository.save(Doctor(doctorDto.id,
                doctorDto.department,
                doctorDto.patients.map { patientDto -> this.patientsService.insertPatient(patientDto)}))
    }
}