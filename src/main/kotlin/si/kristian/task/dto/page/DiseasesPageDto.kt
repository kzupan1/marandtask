package si.kristian.task.dto.page

import si.kristian.task.dto.DiseaseDto
import java.io.Serializable
import javax.xml.bind.annotation.*

@XmlRootElement(name="page")
@XmlAccessorType(XmlAccessType.FIELD)
class DiseasesPageDto: PageDto<DiseaseDto>, Serializable {

    @XmlElementWrapper(name = "diseases")
    @XmlElement(name = "disease")
    var diseases: List<DiseaseDto> = arrayListOf()

    constructor() {}

    constructor(diseases: List<DiseaseDto>, pageNumber: Int, pageSize: Int, total: Long): super(pageNumber, pageSize, total) {
        this.diseases = diseases
    }

}