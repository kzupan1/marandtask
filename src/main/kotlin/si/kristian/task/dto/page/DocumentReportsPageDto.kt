package si.kristian.task.dto.page

import si.kristian.task.dto.DocumentReportDto
import java.io.Serializable
import javax.xml.bind.annotation.*

@XmlRootElement(name="page")
@XmlAccessorType(XmlAccessType.FIELD)
class DocumentReportsPageDto: PageDto<DocumentReportDto>, Serializable {

    @XmlElementWrapper(name = "documentReports")
    @XmlElement(name = "documentReport")
    var documentReports: List<DocumentReportDto> = arrayListOf()

    constructor() {}

    constructor(documentReports: List<DocumentReportDto>, pageNumber: Int, pageSize: Int, total: Long): super(pageNumber, pageSize, total) {
        this.documentReports = documentReports
    }

}