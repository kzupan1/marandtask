package si.kristian.task.exceptions

import java.util.*

class DuplicateEntityException (val id: Int, message: String): Exception(message) {
    var doctorId: Int? = null
    var executionStartTime: Date? = null
}