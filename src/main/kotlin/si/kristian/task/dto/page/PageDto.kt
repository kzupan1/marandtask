package si.kristian.task.dto.page

import io.swagger.annotations.ApiModel
import si.kristian.task.dto.DataTransferObject
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlTransient

@ApiModel(value = "page")
@XmlTransient
abstract class PageDto<T: DataTransferObject> {

    @XmlElement
    var pageNumber: Int = 0

    @XmlElement
    var pageSize: Int = 10

    @XmlElement
    var total: Long = 0

    constructor()
    constructor(pageNumber: Int, pageSize: Int, total: Long) {
        this.pageNumber = pageNumber
        this.pageSize = pageSize
        this.total = total
    }


}