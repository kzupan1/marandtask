package si.kristian.task.web

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import si.kristian.task.domain.Patient
import si.kristian.task.dto.PatientDto
import si.kristian.task.service.PatientsService

@RestController
@RequestMapping("/api/patients")
class PatientsController(override val service: PatientsService): GetController<Patient, PatientDto>(service)