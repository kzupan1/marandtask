package si.kristian.task.repository

import org.springframework.data.repository.PagingAndSortingRepository
import si.kristian.task.domain.Patient

interface PatientRepository: PagingAndSortingRepository<Patient, Int>