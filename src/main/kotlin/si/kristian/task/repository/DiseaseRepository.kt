package si.kristian.task.repository

import org.springframework.data.repository.PagingAndSortingRepository
import si.kristian.task.domain.Disease
import java.util.*

interface DiseaseRepository: PagingAndSortingRepository<Disease, Int> {
    fun findByName(name: String): Optional<Disease>
}