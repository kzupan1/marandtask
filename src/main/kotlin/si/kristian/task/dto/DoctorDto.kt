package si.kristian.task.dto

import io.swagger.annotations.ApiModel
import java.io.Serializable
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@ApiModel(value = "doctor")
@XmlRootElement(name = "doctor")
@XmlAccessorType(XmlAccessType.FIELD)
class DoctorDto : Serializable, DataTransferObject {

    @XmlAttribute(required = true)
    @get: Min(1)
    var id: Int = 0

    @XmlAttribute(required = true)
    @get: [NotNull NotBlank]
    var department: String = ""

    @XmlElementWrapper(name = "patients")
    @XmlElement(name = "patient", required = true)
    @get: [NotEmpty]
    @Valid
    var patients: List<PatientDto> = arrayListOf()

    constructor() {}

    constructor(id: Int, department: String, patients: List<PatientDto>) {
        this.id = id
        this.department = department
        this.patients = patients
    }

}
