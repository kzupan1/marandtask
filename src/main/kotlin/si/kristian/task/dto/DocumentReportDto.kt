package si.kristian.task.dto

import io.swagger.annotations.ApiModel
import java.io.Serializable
import java.util.*
import javax.xml.bind.annotation.*

@ApiModel(value = "documentReport")
@XmlRootElement(name = "documentReport")
@XmlAccessorType(XmlAccessType.FIELD)
class DocumentReportDto: Serializable, DataTransferObject {

    @XmlElement
    var id: Int = 0

    @XmlElement
    var doctorId : Int = 0

    @XmlElement
    var executionStartTime: Date? = null

    @XmlElement
    var error: String? = null

    constructor(){}

    constructor(id: Int, doctorId: Int, executionStartTime: Date?) {
        this.id = id
        this.doctorId = doctorId
        this.executionStartTime = executionStartTime
    }

    constructor(id: Int, doctorId: Int, executionStartTime: Date?, error: String?) {
        this.id = id
        this.doctorId = doctorId
        this.executionStartTime = executionStartTime
        this.error = error
    }
}