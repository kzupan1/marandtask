package si.kristian.task.dto.page

import si.kristian.task.dto.DoctorDto
import java.io.Serializable
import javax.xml.bind.annotation.*

@XmlRootElement(name="page")
@XmlAccessorType(XmlAccessType.FIELD)
class DoctorsPageDto: PageDto<DoctorDto>, Serializable {

    @XmlElementWrapper(name = "doctors")
    @XmlElement(name = "doctor")
    var doctors: List<DoctorDto> = arrayListOf()

    constructor() {}

    constructor(doctors: List<DoctorDto>, pageNumber: Int, pageSize: Int, total: Long): super(pageNumber, pageSize, total) {
        this.doctors = doctors
    }

}