package si.kristian.task.web

import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import si.kristian.task.dto.errors.ApiError
import si.kristian.task.dto.errors.ApiSubError
import si.kristian.task.exceptions.DoctorProcessingException
import si.kristian.task.exceptions.DuplicateEntityException
import si.kristian.task.exceptions.EntityNotFoundException
import si.kristian.task.service.DocumentReportsService
import java.util.*


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class RestExceptionHandler(val documentReportsService: DocumentReportsService) : ResponseEntityExceptionHandler() {

    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val error = "Malformed request"
        return buildResponseEntity(ApiError(HttpStatus.BAD_REQUEST, error, ex))
    }

    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException, headers: HttpHeaders , status: HttpStatus, request: WebRequest ): ResponseEntity<Any> {
        val error = "Malformed request"
        val subErrors = ex.bindingResult.allErrors
                .map { e -> ApiSubError((e as FieldError).field, e.defaultMessage) }

        return buildResponseEntity(ApiError(HttpStatus.BAD_REQUEST, error, ex, subErrors))
    }

    private fun buildResponseEntity(apiError: ApiError): ResponseEntity<Any> {
        return ResponseEntity(apiError, apiError.status)
    }

    @ExceptionHandler(DoctorProcessingException::class)
    fun handleDoctorProcessingException(ex: DoctorProcessingException): ResponseEntity<Any> {
        storeExceptionReport(ex.doctorId, ex.executionStartTime, ex.localizedMessage)
        return buildResponseEntity(ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error at processing doctor", ex))

    }

    @ExceptionHandler(EntityNotFoundException::class)
    fun handleEntityNotFoundException(ex: EntityNotFoundException): ResponseEntity<Any> {

        val error = "Cannot find entity with id: ${ex.id}"
        return buildResponseEntity(ApiError(HttpStatus.NOT_FOUND, error, ex))

    }

    @ExceptionHandler(DuplicateEntityException::class)
    fun handleDuplicateEntityException(ex: DuplicateEntityException): ResponseEntity<Any> {
        storeExceptionReport(ex.doctorId!!, ex.executionStartTime!!, ex.localizedMessage)
        val error = "Entity with id: ${ex.id} already exists"
        return buildResponseEntity(ApiError(HttpStatus.CONFLICT, error, ex))

    }

    fun storeExceptionReport(doctorId: Int, executionStartTime: Date, errorMessage: String?) {
        try {
            this.documentReportsService.storeReport(doctorId, executionStartTime, errorMessage)
        } catch (e: Exception) {
            this.logger.error("Error in saving document report error ", e)
        }
    }


}