package si.kristian.task.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import si.kristian.task.domain.Doctor
import si.kristian.task.domain.DocumentReport
import si.kristian.task.dto.DocumentReportDto
import si.kristian.task.dto.page.DoctorsPageDto
import si.kristian.task.dto.page.DocumentReportsPageDto
import si.kristian.task.dto.page.PageDto
import si.kristian.task.repository.DocumentReportRepository
import java.util.*

@Service
class DocumentReportsService(override val repository: DocumentReportRepository): GetService<DocumentReport, DocumentReportDto>(repository) {
    override fun mapPageToPageDto(page: Page<DocumentReport>, pageable: Pageable) =
            DocumentReportsPageDto(page.content.map { e -> this.mapToDto(e) }, pageable.pageNumber, pageable.pageNumber, page.totalElements)

    override fun mapToDto(entity: DocumentReport): DocumentReportDto {
        return DocumentReportDto(entity.id, entity.doctorId, entity.executionStartTime, entity.error)
    }

    fun storeReport(doctorId: Int, executionStartTime: Date = Date(), error: String? = null) {
        this.repository.save(DocumentReport(doctorId, executionStartTime, error))
    }
}