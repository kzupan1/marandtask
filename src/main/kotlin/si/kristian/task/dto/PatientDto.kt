package si.kristian.task.dto

import io.swagger.annotations.ApiModel
import java.io.Serializable
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@ApiModel(value = "patient")
@XmlRootElement(name = "patient")
@XmlAccessorType(XmlAccessType.FIELD)
class PatientDto : Serializable, DataTransferObject {

    @XmlElement(required = true)
    @get: Min(1)
    var id: Int = 0

    @XmlElement(name = "first_name", required = true)
    @get: [NotNull NotBlank]
    var firstName: String = ""

    @XmlElement(name = "last_name", required = true)
    @get: [NotNull NotBlank]
    var lastName: String = ""


    @XmlElementWrapper(name = "diseases")
    @XmlElement(name = "disease", required = true)
    @get: NotEmpty
    @Valid
    var diseases: List<DiseaseDto> = arrayListOf()


    constructor() {}

    constructor(id: Int, firstName: String, lastName: String, diseases: List<DiseaseDto>) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.diseases = diseases
    }
}
