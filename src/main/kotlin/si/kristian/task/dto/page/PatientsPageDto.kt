package si.kristian.task.dto.page

import si.kristian.task.dto.PatientDto
import java.io.Serializable
import javax.xml.bind.annotation.*

@XmlRootElement(name="page")
@XmlAccessorType(XmlAccessType.FIELD)
class PatientsPageDto: PageDto<PatientDto>, Serializable {

    @XmlElementWrapper(name = "patients")
    @XmlElement(name = "patient")
    var patients: List<PatientDto>? = arrayListOf()

    constructor() {}

    constructor(patients: List<PatientDto>, pageNumber: Int, pageSize: Int, total: Long): super(pageNumber, pageSize, total) {
        this.patients = patients
    }

}