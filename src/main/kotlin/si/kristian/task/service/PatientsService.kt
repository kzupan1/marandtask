package si.kristian.task.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import si.kristian.task.domain.Doctor
import si.kristian.task.domain.Patient
import si.kristian.task.dto.PatientDto
import si.kristian.task.dto.page.DoctorsPageDto
import si.kristian.task.dto.page.PageDto
import si.kristian.task.dto.page.PatientsPageDto
import si.kristian.task.exceptions.DuplicateEntityException
import si.kristian.task.repository.PatientRepository

@Service
class PatientsService (override val repository: PatientRepository,
                       val diseaseService: DiseasesService): GetService<Patient, PatientDto>(repository) {
    override fun mapPageToPageDto(page: Page<Patient>, pageable: Pageable) =
            PatientsPageDto(page.content.map { e -> this.mapToDto(e) }, pageable.pageNumber, pageable.pageNumber, page.totalElements)

    override fun mapToDto(entity: Patient): PatientDto {
        val diseases = entity.diseases!!.map { d -> this.diseaseService.mapToDto(d) }
        return PatientDto(entity.id, entity.firstName, entity.lastName, diseases)
    }

    @Throws(DuplicateEntityException::class)
    fun validateBL(patientDto: PatientDto) {
        if (this.repository.existsById(patientDto.id)) {
            throw DuplicateEntityException(patientDto.id, "Patient with: ${patientDto.id} already exists")
        }
    }

    @Transactional
    fun insertPatient(patientDto: PatientDto): Patient {
        return this.repository.save(Patient (patientDto.id, patientDto.firstName, patientDto.lastName,
                patientDto.diseases.map { d -> this.diseaseService.upsertDisease(d.name) }))

    }

}