package si.kristian.task

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional
import si.kristian.task.dto.DoctorDto
import si.kristian.task.dto.page.DoctorsPageDto
import si.kristian.task.dto.page.DocumentReportsPageDto
import si.kristian.task.repository.DiseaseRepository
import si.kristian.task.repository.DoctorRepository
import si.kristian.task.repository.DocumentReportRepository


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTests(
        @Autowired val restTemplate: TestRestTemplate,
        @Autowired val doctorRepository: DoctorRepository,
        @Autowired val diseaseRepository: DiseaseRepository,
        @Autowired val documentReportRepository: DocumentReportRepository) {

    @AfterEach
    @Transactional
    fun clearData() {
        this.doctorRepository.deleteAll()
        this.diseaseRepository.deleteAll()
        this.documentReportRepository.deleteAll()
    }

    val testReqBody =
            """
                    <doctor id="100" department="marand">
                        <patients>
                            <patient>
                                <id>1</id>
                                <first_name>Bostjan</first_name>
                                <last_name>Lah</last_name>
                                <diseases>
                                    <disease>nice_to_people</disease>
                                    <disease>long_legs</disease>
                                </diseases>
                            </patient>
                            <patient>
                                <id>2</id>
                                <first_name>Boris</first_name>
                                <last_name>Marn</last_name>
                                <diseases>
                                    <disease>used_to_have_dredds</disease>
                                    <disease>nice_to_people</disease>
                                </diseases>
                            </patient>
                            <patient>
                                <id>3</id>
                                <first_name>Anze</first_name>
                                <last_name>Droljc</last_name>
                                <diseases>
                                    <disease>chocaholic</disease>
                                    <disease>great_haircut</disease>
                                </diseases>
                            </patient>
                        </patients>
                    </doctor>
                """.trimIndent()

    val secondDoctorReqBody =
            """
                    <doctor id="101" department="marand">
                        <patients>
                            <patient>
                                <id>4</id>
                                <first_name>Kristian</first_name>
                                <last_name>Zupan</last_name>
                                <diseases>
                                    <disease>nice_to_people</disease>
                                    <disease>long_legs</disease>
                                </diseases>
                            </patient>
                        </patients>
                    </doctor>
                """.trimIndent()


    @Test
    fun `Empty doctors page`() {
        val entity = restTemplate.getForEntity<DoctorsPageDto>("/api/doctors")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isNotNull
        assertThat(entity.body!!.total).isEqualTo(0)
    }

    @Test
    fun `Post test example`() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity(testReqBody, headers)

        val entity = restTemplate.postForEntity<DoctorDto>("/api/doctors", request)

        assertThat(entity.statusCode).isEqualTo(HttpStatus.CREATED)
        val doctor = entity.body
        assertThat(doctor).isNotNull

        assertThat(doctor!!.id).isEqualTo(100)
        assertThat(doctor!!.department).isEqualTo("marand")

        assertThat(doctor!!.patients).isNotEmpty
        assertThat(doctor!!.patients.size).isEqualTo(3)

        val patient1 = doctor.patients.find { p -> p.id == 1 }

        assertThat(patient1).isNotNull
        assertThat(patient1!!.firstName).isEqualTo("Bostjan")
        assertThat(patient1!!.lastName).isEqualTo("Lah")
        assertThat(patient1!!.diseases).isNotEmpty
        assertThat(patient1!!.diseases.size).isEqualTo(2)

        val disease1 = patient1.diseases.find { d -> d.name == "nice_to_people" }

        assertThat(disease1).isNotNull
    }

    @Test
    fun `Should return 400 because of missing properties`() {

        val invalidBodyString =
                """
                    <doctor department="marand">
                    </doctor>
                """.trimIndent()


        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity(invalidBodyString, headers)

        val entity = restTemplate.postForEntity<DoctorDto>("/api/doctors", request)

        assertThat(entity.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
    }

    @Test
    fun `Should return 409 because of posting doctor with existing id`() {

        val conflictDoctorBodyString =
                """
                    <doctor id="100" department="marand">
                        <patients>
                            <patient>
                                <id>1</id>
                                <first_name>Bostjan</first_name>
                                <last_name>Lah</last_name>
                                <diseases>
                                    <disease>nice_to_people</disease>
                                    <disease>long_legs</disease>
                                </diseases>
                            </patient>
                        </patients>
                    </doctor>
                """.trimIndent()


        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML

        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(testReqBody, headers))
        val entity =
                restTemplate.postForEntity<DoctorDto>("/api/doctors",  HttpEntity(conflictDoctorBodyString, headers))

        assertThat(entity.statusCode).isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    fun `Should return 409 because of posting patient with existing id`() {

        val conflictDoctorBodyString =
                """
                    <doctor id="101" department="marand">
                        <patients>
                            <patient>
                                <id>1</id>
                                <first_name>Bostjan</first_name>
                                <last_name>Lah</last_name>
                                <diseases>
                                    <disease>nice_to_people</disease>
                                    <disease>long_legs</disease>
                                </diseases>
                            </patient>
                        </patients>
                    </doctor>
                """.trimIndent()


        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML

        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(testReqBody, headers))
        val entity =
                restTemplate.postForEntity<DoctorDto>("/api/doctors",  HttpEntity(conflictDoctorBodyString, headers))

        assertThat(entity.statusCode).isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    fun `Should get doctor by id`() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML

        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(testReqBody, headers))
        val entity = restTemplate.getForEntity<DoctorDto>("/api/doctors/100")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isNotNull
        assertThat(entity.body!!.id).isEqualTo(100)
    }


    @Test
    fun `Doctors page with results`() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML

        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(testReqBody, headers))
        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(secondDoctorReqBody, headers))
        val entity = restTemplate.getForEntity<DoctorsPageDto>("/api/doctors")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isNotNull
        assertThat(entity.body!!.total).isEqualTo(2)
    }

    @Test
    fun `Should get document reports`() {

        val conflictDoctorBodyString =
                """
                    <doctor id="101" department="marand">
                        <patients>
                            <patient>
                                <id>1</id>
                                <first_name>Bostjan</first_name>
                                <last_name>Lah</last_name>
                                <diseases>
                                    <disease>nice_to_people</disease>
                                    <disease>long_legs</disease>
                                </diseases>
                            </patient>
                        </patients>
                    </doctor>
                """.trimIndent()



        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML

        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(testReqBody, headers))
        restTemplate.postForEntity<DoctorDto>("/api/doctors", HttpEntity(conflictDoctorBodyString, headers))
        val entity = restTemplate.getForEntity<DocumentReportsPageDto>("/api/document-reports")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isNotNull
        assertThat(entity.body!!.total).isEqualTo(2)

        val errorReport = entity.body!!.documentReports.find { dr -> dr.error != null}
        assertThat(errorReport).isNotNull
        assertThat(errorReport!!.executionStartTime).isNotNull()
        assertThat(errorReport!!.doctorId).isEqualTo(101)
        assertThat(errorReport!!.error).isNotEmpty()

    }
}
