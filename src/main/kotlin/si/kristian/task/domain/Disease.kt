package si.kristian.task.domain

import javax.persistence.*

@Entity
data class Disease(
        @Column(nullable = false, unique = true)
        val name: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        override val id: Int = 0
): BaseEntity()
