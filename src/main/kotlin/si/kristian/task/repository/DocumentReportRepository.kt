package si.kristian.task.repository

import org.springframework.data.repository.PagingAndSortingRepository
import si.kristian.task.domain.DocumentReport

interface DocumentReportRepository: PagingAndSortingRepository<DocumentReport, Int>