package si.kristian.task.dto.errors

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "subError")
@XmlAccessorType(XmlAccessType.FIELD)
class ApiSubError {

    @XmlElement
    var field: String? = null

    @XmlElement
    var message: String? = null

    constructor() {}

    constructor(field: String?) {
        this.field = field
    }

    constructor(field: String?, message: String?) {
        this.field = field
        this.message = message
    }
}