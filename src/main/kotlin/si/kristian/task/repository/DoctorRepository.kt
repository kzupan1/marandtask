package si.kristian.task.repository

import org.springframework.data.repository.PagingAndSortingRepository
import si.kristian.task.domain.Doctor

interface DoctorRepository: PagingAndSortingRepository<Doctor, Int>