package si.kristian.task.web

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import si.kristian.task.domain.Disease
import si.kristian.task.dto.DiseaseDto
import si.kristian.task.service.DiseasesService

@RestController
@RequestMapping("/api/diseases")
class DiseasesController(override val service: DiseasesService): GetController<Disease, DiseaseDto>(service)