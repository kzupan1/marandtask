package si.kristian.task.web

import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import si.kristian.task.domain.BaseEntity
import si.kristian.task.dto.DataTransferObject
import si.kristian.task.dto.page.PageDto
import si.kristian.task.service.GetService


abstract class GetController<T: BaseEntity, U: DataTransferObject>(open val service: GetService<T,U>) {

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_XML_VALUE])
    fun getOne(@PathVariable id: Int): ResponseEntity<U> {
        val dto = this.service.mapToDto(this.service.getOne(id))
        return ResponseEntity(dto, HttpStatus.OK)
    }

    @RequestMapping(method = [RequestMethod.GET], produces = [MediaType.APPLICATION_XML_VALUE])
    fun findAll(@RequestParam(defaultValue = "0") page: Int, @RequestParam(defaultValue = "10") size: Int): ResponseEntity<PageDto<U>> {
        val pageable  = PageRequest.of(page, size)
        val pagedResult = this.service.findAll(pageable)
        val dtoPage = this.service.mapPageToPageDto(pagedResult, pageable)
        return ResponseEntity(dtoPage, HttpStatus.OK)
    }

}
