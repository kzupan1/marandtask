package si.kristian.task.domain

import javax.persistence.*

@Entity
data class Patient(
        @Id
        override val id: Int,

        @Column(nullable = false)
        val firstName: String,

        @Column(nullable = false)
        val lastName: String,

        @ManyToMany
        var diseases: List<Disease>? = null
): BaseEntity()
